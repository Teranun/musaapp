import 'package:flutter/material.dart';

void main() {
  runApp(Nav2App());
}

class Nav2App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: (settings) {
        // Handle '/'
        if (settings.name == '/') {
          return MaterialPageRoute(builder: (context) => HomeScreen());
        }

        // Handle '/details/:id'
        var uri = Uri.parse(settings.name);
        if (uri.pathSegments.length == 2 &&
            uri.pathSegments.first == 'details') {
          var id = uri.pathSegments[1];
          return MaterialPageRoute(builder: (context) => DetailScreen(id: id));
        }

        return MaterialPageRoute(builder: (context) => UnknownScreen());
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFFF57617),
          title: Text('ลงชื่อกิจกรรม'),
        ),
        body: ListView(children: [
          Image.asset(
            'images/qrcode.jpg',
            width: 900,
            height: 600,
          ),
          Text(
            'ลงชื่อเข้ากิจกรรม',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'กดสแกน QRCODE เพื่อเข้าสู่หน้าถัดไป',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey[5000],
            ),
          ),
          Center(
            child: FlatButton(
              child: Text('ตกลง'),
              color: Color(0xFFF57617),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/details/1',
                );
              },
            ),
          ),
        ]));
  }
}

class DetailScreen extends StatelessWidget {
  String id;

  DetailScreen({
    this.id,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFF57617),
        title: Text('ลงชื่อกิจกรรม'),
      ),
      body: ListView(children: [
        Image.asset(
          'images/001.jpg',
          width: 300,
          height: 200,
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text('กิจกรรมรับน้อง', style: TextStyle(fontSize: 36)),
                ],
              ),
              Row(
                children: [
                  Text(
                    'รายละเอียด :_________________________________________',
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 350),
              ),
              FlatButton(
                child: Text('กลับหน้าหลัก'),
                color: Color(0xFFF57617),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ]),
    );
  }
}

class UnknownScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Text('404!'),
      ),
    );
  }
}
